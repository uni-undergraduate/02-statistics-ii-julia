
function mergefiles(input, output)
    text = vcat(map(readlines, input)...)
    open(output,"w") do file
        for line in text
            println(file, line)
        end
    end
end

function mergefolder(folder, output)
    input = filter(x -> occursin(r"^[0-9]+.*\.jl$", x), readdir(folder)) |>
        x -> joinpath.(folder, x)
    mergefiles(input, output)
end



