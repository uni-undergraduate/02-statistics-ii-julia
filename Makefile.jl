using Weave

# load functions

include(joinpath("src", "files.jl"))

# get input and output paths

path_from = joinpath("..", "02-statistics-ii", "scripts", "code")
path_to = joinpath("scripts")

folders = readdir(path_from) |>
    z -> filter(x -> occursin(r"[0-9]+-.*$", x), z)

path_inputs = joinpath.(path_from, folders)
path_outputs = joinpath.(path_to, folders .* ".jl")

# join jl files

mergefolder.(path_inputs, path_outputs)

# convert merged scripts to notebooks

jmds = filter(x -> occursin(r"[0-9]+-.*\.jl$", x), readdir("scripts", join = true))
ipynbs = replace.(replace.(jmds, "scripts" => "notebooks"), ".jl" => ".ipynb")
convert_doc.(jmds, ipynbs)

