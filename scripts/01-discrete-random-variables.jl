#' ---
#' title: Basic concepts of discrete random variables
#' author: Erick A. Chacón-Montalván
#' weave_options:
#'   term: true
#' ---

#' ## Conceptos básicos de variables aleatorias discretas
#'
#' En esta sección, abarcaremos los conceptos básicos asociados a variables aleatorias
#' como *función de masa de probabilidad*, *función de distribución acumulada*,
#' *parámetros*, medidas de centralidad y variabilidad.


#' ### Paquetes and opciones
using Plots
using Distributions
gr(size = (600, 420), dpi = 300)

#' ### Ejemplo de dados
#' Sea la variable aleatoria ganancia (X) igual a 10 veces el resultado de un dado.

Ω = [1, 2, 3, 4, 5, 6]
prob = [1/6, 1/6, 1/6, 1/6, 1/6, 1/6]
x = Ω * 50

#' La función de masa de probabilidad se puede graficar como se muestra a continuación.

plot(x, prob, seriestype = :scatter, ylim = [0, 1])

#' Utiliza los siguienter argumentos para mejorar la gráfica:
#' - `color = "red"`
#' - `xlab = "x"`
#' - `ylab = "Pr(X = x)"`
#' - `title = "Función de masa de probabilidad"`
#' - `legend = "false"`

#' La función de distribución acumulada es.

probacum = [1/6, 2/6, 3/6, 4/6, 5/6, 6/6]
plot(x, probacum, seriestype = :scatter, ylim = [0, 1])

#' Imagine la situación en la que la proabilidad de observar 1 en el dado es $\theta$.

θ = 0.1
prob = [θ, (1-θ) / 5, (1-θ) / 5, (1-θ) / 5, (1-θ) / 5, (1-θ) / 5]
plot(x, prob, seriestype = :scatter, ylim = [0, 1])
probacum = cumsum(prob)
plot(x, probacum, seriestype = :scatter, ylim = [0, 1])

#' Se puede calcular medidas de centralidad y variabilidad.

#' # media

media = x[1] * prob[1] + x[2] * prob[2] +
x[3] * prob[3] + x[4] * prob[4] +
x[5] * prob[5] + x[6] * prob[6]

[x prob (x .* prob)]

sum(x .* prob)

#' # varianza

varianza = (x[1] - media) ^ 2 * prob[1] + (x[2] - media) ^ 2 * prob[2] +
(x[3] - media) ^ 2 * prob[3] + (x[4] - media) ^ 2 * prob[4] +
(x[5] - media) ^ 2 * prob[5] + (x[6] - media) ^ 2 * prob[6]

[x (x .- media) (x .- media) .^ 2 prob]


#' ---
#' title: Mean Interpretation
#' author: Erick A. Chacón-Montalván
#' weave_options:
#'   term: true
#' ---

#' ## Interpretación de la media

#' ### Cargamos los paquetes requeridos

using Plots

#' ### Definimos un experimento y su variable aleatoria

#' Sea la variable aleatoria en la que un jugador lanza el dado y gana 50 veces el número
#' que le salió. Definimos el espacio muestral del experimento y el dominio de la variable
#' aleatoria.

Ω = [1, 2, 3, 4, 5, 6]
x = Ω * 50
prop = [1/6, 1/6, 1/6, 1/6, 1/6, 1/6]

#' La media se obtiene como:

sum(x .* prop)

#' ### Interpretación

#' Al realizar el experimento un gran número de veces, la genancia esperada será el valor
#' medio calculado anteriormente.

#' Para $N = 10$

N = 10
realizaciones = rand(x, N)
sum(realizaciones) / N

#' Para $N = 100$

N = 100
realizaciones = rand(x, N)
sum(realizaciones) / N

#' Para $N = 1000$

N = 1000
realizaciones = rand(x, N)
sum(realizaciones) / N

#' Para $N = 10000$

N = 10000
realizaciones = rand(x, N)
sum(realizaciones) / N

#' Para $N = 100000$

N = 100000
realizaciones = rand(x, N)
sum(realizaciones) / N

#' ---
#' title: Binomial Random Variable
#' author: Erick A. Chacón-Montalván
#' weave_options:
#'   term: true
#' ---

#' ## Variable Aleatoria Binomial

#' ### Paquetes and opciones

using Plots
using Distributions
gr(size = (600, 420), dpi = 300)

#' ### Variable aleatoria

#' Definimos la variable aleatoria.

X = Binomial(10, 0.3)

#' Definimos el rango (posibles valores) de la variable aleatoria. Es decir los posibles
#' números de exito.

k = 0:10

#' ### Teoricamente

#' Evaluamos la función de masa de probabilidad (pdf) en los posibles números de éxitos. Y
#' lo graficamos.

fmp_k = pdf(X, k)
plot(k, fmp_k, seriestype = :bar, title = "Función de masa de probabilidad",
    xlab = "k", ylab = "Pr(X = k)")

#' Evaluamos la función de distribución acumulada (cdf) en los posibles números de éxitos. Y
#' lo graficamos.

fda_k = cdf(X, k)
plot(k, fda_k, seriestype = :step, title = "Función de distribución acumulada",
    xlab = "k", ylab = "Pr(X <= k)")

#' ### Observaciones

#' Al simular observaciones de esta variable aleatoria, podemos notar que la frecuencia de
#' valores observados es similar a función de masa de probabilidad.

#' Simulamos observaciones.
observaciones = rand(X, 1000)

#' Calcular la frecuencia observada para cada posible valor.
contar(x) = sum(observaciones .== x)
k_count = map(contar, k)
[k k_count]

#' Graficamos.
plot(k, k_count ./ 1000, seriestype = :bar, title = "Frecuencia Relativa Observada",
     xticks = k)

